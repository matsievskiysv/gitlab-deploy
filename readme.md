Gitlab self hosted server with runner with self-signed certificates.

# Configure

Set options in [`.env`](./.env) file.

Create self-signed certificates
```bash
./gen_certs.sh
```

In order to connect to gitlab users must import CA certificate from `./gitlab_files/cert/<company_name>_ca.crt`

Optionally, change initial passwords
```
gen_passwords.sh
```

Initial `root` password will be changed in `.env` file.


# Start

## Bring up

```bash
docker-compose up -d
```

## View logs

```bash
docker-compose logs --follow
```

## Pause and continue

```bash
docker-compose stop
```

```bash
docker-compose start
```

## Pause and continue

```bash
docker-compose stop
```

```bash
docker-compose start
```

# Connect runner

After gitlab start go to Menu->Admin Overview->Runners; click on `Register an instance runner` and copy token.
In terminal run
```bash
./register_runner.sh
```
and follow interactive setup.

# Backup and restore

## Backup
```bash
./backup_create.sh
```

Note that files from `gitlab_files/config/` are not included in backup and should be saved manually.

## Restore
```bash
./backup_restore.sh <version>
```
where `<version>` is a backup filename without `_gitlab_backup.tar` suffix.
Backup files are created in `gitlab_files/data/backups/`

# Links

Docker images CE: https://hub.docker.com/r/gitlab/gitlab-ce/

Docker images EE: https://hub.docker.com/r/gitlab/gitlab-ee/

Stable release version number: https://about.gitlab.com/releases/categories/releases/

Upgrade path: https://docs.gitlab.com/ee/update/index.html#upgrading-to-a-new-major-version

Config template: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template

Backup: https://docs.gitlab.com/ee/raketasks/backup_restore.html
