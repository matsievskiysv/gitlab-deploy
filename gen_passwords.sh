#!/usr/bin/env bash

TOKEN_FILE=runner_files/config.toml

PASS=$(openssl rand -hex 16)
TOKEN=$(openssl rand -hex 16)
HOST=$(grep 'HOSTNAME' .env | grep -Eo '[0-9.]+')
PORT=$(grep 'HTTP_PORT' .env | grep -Eo '[0-9]+')

if [ -f .env ]; then
    sed -i "s|ROOT_PASSWORD.*|ROOT_PASSWORD=${PASS}|g" .env
    sed -i "s|RUNNER_TOKEN.*|RUNNER_TOKEN=${TOKEN}|g" .env
else
    echo .env does not exist!
fi

if [ -f "${TOKEN_FILE}" ]; then
    sed -i "s|token.*|token=\"${TOKEN}\"|g" "${TOKEN_FILE}"
else
    echo "${TOKEN_FILE}" does not exist!
fi
