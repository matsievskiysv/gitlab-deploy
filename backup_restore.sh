#!/usr/bin/env bash

set -e

if [ $1 != "" ]; then

    # Stop the processes that are connected to the database
    docker exec -it gitlab-deploy_gitlab_1 gitlab-ctl stop puma
    docker exec -it gitlab-deploy_gitlab_1 gitlab-ctl stop sidekiq

    # Verify that the processes are all down before continuing
    docker exec -it gitlab-deploy_gitlab_1 gitlab-ctl status

    # Run the restore. NOTE: "_gitlab_backup.tar" is omitted from the name
    docker exec -it gitlab-deploy_gitlab_1 gitlab-backup restore BACKUP=${VERSION}

    # Restart the GitLab container
    docker-compose stop
    docker-compose start -d

    # Check GitLab
    docker exec -it gitlab-deploy_gitlab_1 gitlab-rake gitlab:check SANITIZE=true

else

    echo Usage: ./backup_restore.sh <version>

fi
