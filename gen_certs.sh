#!/usr/bin/env bash

set -e

DAYS=3650

CERT_PATH=gitlab_files/cert

NAME=gitlab
CA_NAME=mycompany.ca

CA_BASENAME=mycompany_ca
CERT_BASENAME=gitlab

PASS_FILE=cert_auth_password

CORPORATION=MyCompany
GROUP=WG1
CITY=Moscow
STATE=Moscow
COUNTRY=RU

ALT_NAMES="[alt_names]
DNS.1 = gitlab
IP.1 = 192.168.2.144
"

if [ -f "$PASS_FILE" ]; then
    CERT_AUTH_PASS=$(cat $PASS_FILE)
else
    CERT_AUTH_PASS=$(openssl rand -base64 32)
    echo $CERT_AUTH_PASS > $PASS_FILE
fi

# create the certificate authority
if [ ! -f "$CERT_PATH/$CA_BASENAME.crt" ] || [ ! -f "$CA_BASENAME.key" ]; then
    openssl \
	req \
	-subj "/CN=${CA_NAME}/OU=${GROUP}/O=${CORPORATION}/L=${CITY}/ST=${STATE}/C=${COUNTRY}" \
	-new \
	-x509 \
	-passout pass:$CERT_AUTH_PASS \
	-keyout $CA_BASENAME.key \
	-out $CERT_PATH/$CA_BASENAME.crt \
	-days $DAYS
fi

# create client private key (used to decrypt the cert we get from the CA)
if [ ! -f "$CERT_PATH/$CERT_BASENAME.key" ]; then
    openssl genrsa -out $CERT_PATH/$CERT_BASENAME.key
fi

if [ ! -f "$CERT_PATH/$CERT_BASENAME.crt" ]; then
    # create the CSR(Certitificate Signing Request)
    openssl \
	req \
	-new \
	-nodes \
	-subj "/CN=${NAME}/OU=${GROUP}/O=${CORPORATION}/L=${CITY}/ST=${STATE}/C=${COUNTRY}" \
	-sha256 \
	-extensions v3_req \
	-reqexts SAN \
	-key $CERT_PATH/$CERT_BASENAME.key \
	-out $CERT_BASENAME.csr \
	-config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=@alt_names\n$ALT_NAMES"))

    # sign the certificate with the certificate authority
    openssl \
	x509 \
	-req \
	-days $DAYS \
	-in $CERT_BASENAME.csr \
	-CA $CERT_PATH/$CA_BASENAME.crt \
	-CAkey $CA_BASENAME.key \
	-CAcreateserial \
	-out $CERT_PATH/$CERT_BASENAME.crt \
	-extfile <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=@alt_names\n$ALT_NAMES")) \
	-extensions SAN \
	-passin pass:$CERT_AUTH_PASS

    rm $CERT_BASENAME.csr

    openssl x509 -in $CERT_PATH/$CERT_BASENAME.crt -text -noout
fi
