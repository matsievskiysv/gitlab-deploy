from aiosmtpd.controller import Controller
from aiosmtpd.handlers import Debugging
from aiosmtpd.smtp import AuthResult, LoginPassword, Envelope, SMTP
from os import environ
from smtplib import SMTP as SMTPclient
import asyncio
import ssl

tasks = [] # global task list

def send_mail(envelope: Envelope):
    try:
        conn = SMTPclient(environ['SMTPSERVER'], int(environ['SMTPPORT']))
        conn.set_debuglevel(False)
        conn.starttls(context=ssl.SSLContext(ssl.PROTOCOL_TLSv1_1))
        conn.login(environ['SMTPUSER'], environ['SMTPPASSWORD'])
        conn.sendmail(envelope.mail_from, envelope.rcpt_tos, envelope.content)
        conn.quit()
        print("Sent mail to ", *envelope.rcpt_tos)
    except Exception as e:
        print(f"Error sending mail: {e}")

async def send_mail_async(envelope: Envelope):
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, send_mail, envelope)

class ControllerTls(Controller):
    def factory(self):
        return SMTP(self.handler, require_starttls=True, tls_context=starttls_context)

class MailHandler:
    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        if not address.endswith(environ['DOMAINFILTER']):
            return '550 not relaying to that domain'
        envelope.rcpt_tos.append(address)
        return '250 OK'

    async def handle_EHLO(self, server, session, envelope, hostname):
        session.host_name = hostname
        return '250-AUTH PLAIN\n250-STARTTLS\n250 HELP'

    async def handle_DATA(self, server, session, envelope):
        tasks.append(asyncio.create_task(send_mail_async(envelope)))
        return '250 Message accepted for delivery'

    async def handle_AUTH(self, server, session, envelope, *args):
        return '235 2.7.0  Authentication Succeeded'

async def wait_for_tasks(tasks):
    while True:
        await asyncio.sleep(10)
        for task in tasks[:]:
            if task.done():
                tasks.remove(task)
                print("Mail sending task finished")

controller = Controller(MailHandler(),
                        "0.0.0.0",
                        int(environ['PROXYPORT']))
controller.start()
asyncio.run(wait_for_tasks(tasks))
