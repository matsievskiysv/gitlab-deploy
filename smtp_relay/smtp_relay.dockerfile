from python:3.9.12

RUN pip3 install --proxy http://127.0.0.1:3128 aiosmtpd
# RUN pip3 install aiosmtpd

COPY relay.py /relay.py
CMD python3 /relay.py
